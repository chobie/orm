<?php
/**
 *
 *	create table moe (moe_date date primary key, number int unsigned);
 */
class Schema_Moe extends ORM_Schema{
	public $moe_date = array(
		"type"=>"date",
		"default"=>null,
		"null"=>false,
		"autoincrement"=>false,
		"primary"=>true
	);
	
	/**
	 *
	 * @type varchar(20)
	 * @default null
	 * @null true
	 */
	public $number = array(
		"type"=>"int",
		"default"=>null,
		"primary"=>false,
		"autoincrement"=>false,
		"null"=>false
	);
	
	protected $__read__  = "test";
	protected $__write__ = "test";
}