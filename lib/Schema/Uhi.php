<?php
/**
 *
 *	create table moe (moe_date date primary key, number int unsigned);
 */
class Schema_Uhi extends ORM_Schema{
	public $id = array(
		"type"=>"int",
		"default"=>null,
		"null"=>false,
		"autoincrement"=>true,
		"primary"=>true
	);
	
	/**
	 *
	 * @type varchar(20)
	 * @default null
	 * @null true
	 */
	public $name = array(
		"type"=>"varchar",
		"default"=>null,
		"primary"=>false,
		"autoincrement"=>false,
		"null"=>false
	);
	
	protected $__read__  = "test";
	protected $__write__ = "test";
}
