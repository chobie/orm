<?php
/**
 *
 *	create table test ( id int unsigned auto_increment, name varchar(20), primary key(id));
 */
class Schema_Test extends ORM_Schema{
	/**
	 *
	 * @type int(10) unsigned
	 * @default null
	 * @null false
	 * @autoincrement true
	 */
	public $id = array(
		"type"=>"int(10) unsigned",
		"default"=>null,
		"null"=>false,
		"autoincrement"=>true,
		"primary"=>true
	);
	
	/**
	 *
	 * @type varchar(20)
	 * @default null
	 * @null true
	 */
	public $name = array(
		"type"=>"varchar(20)",
		"default"=>null,
		"primary"=>false,
		"autoincrement"=>false,
		"null"=>false
	);
	

	public $__relations__ = array(
		"uhi"=>array(
				"local"=>"id",
				"foreign"=>"id",
				"type"=>"has_many"
			)
		);

	protected $__read__  = "test";
	protected $__write__ = "test";
}