<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * ORM_Queryだけだと実装がむずくなるんで
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Query_Join{
	protected $statement;
	protected $relations;

	public function __construct($statement,$params = array()){
		$this->statement = $statement;
		$this->relations = $params;
	}
	
	public function getStatement(){
		//Todo : ここらへんはDriverに任せたい
		foreach($this->relations as $key => $relation){
			return sprintf("%s on %s.%s = %s.%s",$key,$key,$relation["local"],"test",$relation["foreign"]);
			
		}
	}
}