<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * ORM_Queryだけだと実装がむずくなるんで
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Query_From{
	protected $statement;
	//Todo
	// constructでFromを受けてDriver向けにゴニョゴニョする

	public function __construct($statement){
		$this->statement = $statement;
	}
	
	public function getStatement(){
		return $this->statement;
	}
	
	public function getName(){
		// Todo きちんと対象のテーブル名だけ返すように
		return $this->statement;
	}
}