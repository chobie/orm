<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * ORM_Queryだけだと実装がむずくなるんで
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Query_Limit{
	protected $limit;
	protected $offset;
	//Todo
	// constructでLimitする奴を受けてDriver向けにゴニョゴニョする

	public function __construct($limit,$offset = 0){
		$this->limit = $limit;
		$this->offset = $offset;
	}
	
	public function getLimit(){
		return $this->limit;
	}
	
	public function getOffset(){
		return $this->offset;
	}
}