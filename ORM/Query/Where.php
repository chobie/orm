<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * ORM_Queryだけだと実装がむずくなるんで
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Query_Where{
	protected $statement;
	protected $connection;
	protected $glue;

	public function __construct($statement,$params = array(), $glue = 0x01){
		$this->statement = $statement;
		$this->params = $params;
		$this->glue = $glue;
	}
	
	public function getStatement(){
		return $this->statement;
	}
	
	public function getGlue(){
		if($this->glue == 0x01){
			return " and ";
		}else{
			return " or ";
		}
	}
}