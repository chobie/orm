<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * static methodを集めたもの
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Util{
  public static function Cameraized($str){
    return join("",array_map("ucwords",preg_split("/(-|_)/",$str)));
  }

  public static function UnCameraized($str){
    return strtolower(preg_replace_callback("'(?<!^)([A-Z][a-z]*)'",array("ORM_Util",'_UnCameraized'),$str));
  }

  public static function _UnCameraized($matches){
    array_shift($matches);
    $tmp  = "";
    foreach($matches as $match){
      $tmp .= "_" . $match;
    }
    return $tmp;
  }
}