<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * SchemaはORMが正しくテーブル構造を理解するために使われます。
 *
 * Todo:
 * Schema自体は自動生成させてユーザー定義部分はうまくかぶせるようにしたい
 * というか、Schemaはユーザー定義メソッド必要なくね？多分ないと思う
 * 
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Schema{
	const FILTER_THRU = 0x00;
	const FILTER_NO_KEY = 0x01;
	const FILTER_NO_AUTOINCREMENT = 0x02;
	const FILTER_AUTOINCREMENT = 0x03;

	protected $__write__;
	protected $__read__;
	protected $__cache__;
	protected $__name__;
	protected $__relations__;
	

	public function __construct($name = null){
		$classname = get_class($this);
		if($classname == get_class()){
			if(empty($name)){
				throw new Exception("{$classname}::_construct required \$name parameter");
			}
			$this->__name__ = $name;
		}else{
			list($dummy,$name) = explode("_",$classname,2);
			$this->__name__ = strtolower($name);
		}
	}

	public function getName(){
		return $this->__name__;
	}
	public function getPrimaryKeys(){
		$result = array();
		foreach($this as $key=>$row_schema){
			if(strpos($key,"__") === 0){
				continue;
			}
			if($row_schema["primary"]){
				$result[$key] = $row_schema;
			}
		}

		return $result;
	}
	
	public function getColumn($filter = self::FILTER_NO_KEY){
		$result = array();
		foreach($this as $key=>$row_schema){
			if(strpos($key,"__") === 0){
				//管理用プロパティはスキップ
				continue;
			}

			// Todo: 適当すぎるのでそのうちきれいにする
			if($filter == self::FILTER_NO_KEY){
				if(!$row_schema["primary"]){
					$result[$key] = $row_schema;
				}
			}else if($filter == self::FILTER_NO_AUTOINCREMENT){
				if(!$row_schema["autoincrement"]){
					$result[$key] = $row_schema;
				}
			}else if($filter == self::FILTER_AUTOINCREMENT){
				if($row_schema["autoincrement"]){
					$result[$key] = $row_schema;
					// auto_incrementはひとつだけなんで
					break;
				}
			}else{
				$result[$key] = $row_schema;
			}
		}

		return $result;
	}
	
	public function setWriteSession(ORM_Session $session){
		$this->__write__ = $session->getName();
	}

	public function setReadSession(ORM_Session $session){
		$this->__read__ = $session->getName();
	}
	

	/**
	 * Todo: setCacheSessionとかつけたい
	 */
	public function setCacheSession(ORM_Session $session){
		$this->__cache__ = $session->getName();
	}
	
	public function getWriteSession(){
		return ORM_Manager_Session::getConnection($this->__write__);
	}

	public function __set($key,$value){
		$keyname = strtolower($key);
		$this->$keyname = $value;
	}
	
	
	/**
	 * Schemaに関連しているSchemaを返す？
	 * Todo: イケてる仕様を考える。
	 * やりたい事⇒ joinに使う条件が知りたい。
	 */
	public function getRelations(){
		return $this->__relations__;
	}
	
	public static function __set_state(array $array){
		$obj = new ORM_Schema($array["__name__"]);
		foreach($array as $key => $value){
			$obj->$key = $value;
		}

		return $obj;
	}
}