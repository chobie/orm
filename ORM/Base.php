<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * ORM_Driver_*からORM_Sessionを呼び出すためだけのうつわ
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Base{
	// DriverからSession::queryを呼ぶためのダミー
	protected function query($statement){
	}
	
	protected function prepare($statement){
	}

}
