<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * 抽象的なObjectベースのQuery
 *
 * 何を対象にどうするか？ということのみをメタデータとして持ち
 * 実際の作業に関しては全てDriverにゆだねます（今後）。
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
abstract class ORM_Driver extends ORM_Base{
	const HYDRATE_CLASS = 0x01;
	const HYDRATE_ARRAY = 0x02;

}