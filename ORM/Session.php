<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * PDOとの接続・対話を管理します。
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Session extends ORM_Base{
	protected $pdo;
	protected $name;

	protected $driver;
	protected $host;
	protected $user;
	protected $password;
	protected $database;
	protected $port;
	protected $charset = "UTF-8";
	protected $options = array();
	
	
	/**
	 * Schemaを作るのは面倒なので自動生成
	 *
	 * Todo: そのうちきちんとClassとして書き出す。
	 */
	public function createSchema(){
		$driver = $this->loadDriver();
		$schema = array();
		foreach($driver->getTables() as $table_name){
			$a = $driver->desc($table_name);
			$schema[] = "ORM_Manager_Schema::attach(" . var_export($a,true) . ");\r\n";
		}

		return join("\r\n",$schema);
	}
	
	public function loadDriver(){
		$driver = "ORM_Driver_" . ucwords($this->getDriverName());

		//Todo DriverとSessionを疎結合にする
		return new $driver($this);
	}
	
	public function quote($string,$parameter_type = PDO::PARAM_STR){
		$this->has_connect();
		return $this->pdo->quote($string,$parameter_type);
	}
	
	
	private function setDefaultOptions(){
		$this->options = array(
			PDO::ATTR_EMULATE_PREPARES => false,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);
	}
	
	public function getCharset(){
		return $this->charset;
	}
	public function getOptions(){
		// Todo:
		return array();
	}
	public function getDriverName(){
		return $this->driver;
	}
	public function getHostName(){
		return $this->host;
	}
	public function getPort(){
		return $this->port;
	}
	public function getDatabase(){
		return $this->database;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function __construct($url,$name){
		$this->setDefaultOptions();

		$parsed = parse_url($url);
		if(isset($parsed["scheme"])){
			$this->driver = $parsed["scheme"];
		}

		if(isset($parsed["host"])){
			$this->host = $parsed["host"];
		}

		if(isset($parsed["user"])){
			$this->user = $parsed["user"];
		}

		if(isset($parsed["path"])){
			$this->database = ltrim($parsed["path"],"/ ");
		}
		
		if(isset($parsed["port"])){
			$this->port = $parsed["port"];
		}else{
			$this->port = 3306;
		}
		$this->name = $name;
	}
	
	public function connect(){
		if(!isset($this->pdo)){
			$this->pdo = new PDO(
				$this->create_dsn(),
				$this->user,
				$this->password,
				$this->getOptions()
			);
			// deplicated
			//$this->pdo->setAttribute(PDO::ATTR_STATEMENT_CLASS,array("ORM_PDOStatementX"));
		}
	}
	
	public function has_connect($on_failure_connect = true){
		if(!isset($this->pdo)){
			$this->connect();
		}
	}
	
	public function begin(){
		$this->has_connect();
		$this->pdo->beginTransaction();
	}
	
	public function commit(){
		$this->has_connect();
		$this->pdo->commit();
	}
	
	public function rollback(){
		$this->has_connect();
		$this->pdo->rollBack();
	}
	
	public function getErrorNo(){
		$this->has_connect();
		$this->pdo->errorCode();
	}
	
	public function getErrorInfo(){
		$this->has_connect();
		$this->pdo->ErrorInfo();
	}
	
	public function lastInsertId(){
		$this->has_connect();
		return $this->pdo->lastInsertId();
	}
	
	protected function prepare($statement, array $driver_options = array()){
		$this->has_connect();
		return 	$this->pdo->prepare($statement,$driver_options);
	}

	protected function query($statement){
		$this->has_connect();
		return $this->pdo->query($statement);
	}
	
	protected function exec($statement){
		$this->has_connect();
		return $this->pdo->exec($statement);
	}
	
	
	protected function create_dsn(){
		return sprintf("%s:host=%s;port=%d;dbname=%s;charset=%s",
			$this->getDriverName(),
			$this->getHostName(),
			$this->getPort(),
			$this->getDatabase(),
			$this->getCharset()
		);
	}
}
