<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * Model
 *
 * Modelは単数ないし複数のテーブルからの呼び出しをまとめた
 * ひとつのビジネスロジックです。
 * (但し、メインとなるテーブルは常に一つとなります。)
 *
 * Modelでは抽象化されたORM_Queryクラスのみを扱い、直接Sessionに対して
 * Queryを発行してはいけません。（というか出来ないようになってます）
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Model{
	const USE_RELATION = 0x01;

	protected $__name__;
	protected $__session__;

	public function getRecord($mixed = null){
		// Todo: Relationも含めてからのRecordを取るべきか考える
		
		$schema = $this->getSchema();
		$record = clone ORM_Manager_Record::get($schema->getName());

		if(is_array($mixed)){
			foreach($schema->getColumn(ORM_Schema::FILTER_THRU) as $key => $type){
				if(isset($mixed[$key])){
					$record->$key = $mixed[$key];
				}
			}
		}else if(is_object($mixed)){
			foreach($schema->getColumn(ORM_Schema::FILTER_THRU) as $key => $type){
				if(isset($mixed->$key)){
					$record->$key = $mixed->$key;
				}
			}
		}
		return $record;
	}

	public function getSchema(){
		return ORM_Manager_Schema::get($this->getName());
	}
	
	public function getName(){
		return $this->__name__;
	}

	public function __construct($tablename,$session){
		$this->__name__ = $tablename;
		$this->__session__ = $session;
	}
	
	private function getSession(){
		return $this->__session__;
	}
	
	
	/**
	 * Query->from->selectの省略形
	 */
	protected function Select($target, $relation_mode = self::USE_RELATION){
		$q = $this->Query()->from($this->getName())->select($target);

		if($relation_mode == self::USE_RELATION && $relations = $this->getSchema()->getRelations()){
			foreach($relations as $key => $item){
				$q->join($key,array($key => $item));
			}
		}
		return $q;
	}
	
	protected function Query(){
		//Todo Query自体はセッションもつ必要ないと思う。
		// どちらかというとDriverでSelect/Insertの時にSchemaから取得のが良いかと
		return new ORM_Query($this->getSession());
	}
	
	public function find(array $where = array()){
		$q = $this->Select("*");

		if((bool) $where ){
			foreach($where as $key =>$item){

				// Todo: Driver依存にする
				$q->where($key ." = " . $this->getSession()->quote($item));
			}
		}
		
		return $q->execute();
	}

	/**
	 *
	 * Key-ValueなArrayを返す。返したい
	 */
	public function getPair($value_column = null){
	}
}
