<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * ORM_Queryから実際のクエリを生成・発行したり。Adapter的なもの
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Driver_Mysql extends ORM_Driver{

	/**
	 *
	 * Driver依存なフラグを書いとく。
	 */
	const USE_DUPLICATE_KEY_UPDATE = 0x01;

	// Todo: 実装する
	const USE_SELECT_FOR_UPDATE = 0x10;


	static $instance;
	protected $session;
	
	public function getSession(){
		return $this->session;
	}
	
	public static function getInstance(){
		if(isset(self::$instance)){
			return self::$instance;
		}else{
			//　Todo: あれ。なんか違うなこれ。
			// __constructでSession受けるのやめたほうがヨサゲ。
			// 意味的にはDriverはSingletonなんだけどSessionは複数受けられるように
			// しとかないとおかしいと思う。
			//self::$instance = new ORM_Driver_Mysql();
			//return self::$instance;
		}
	}

	public function __construct(ORM_Session $session){
		$this->session = $session;
	}
	
	// TODO: HYDRATE_ASSOCはそのうち
	public function execute(ORM_Query $query,$hydrate_mode = ORM_Driver::HYDRATE_CLASS){
		$statement = $this->getSession()->prepare($query->freeze());

		if((int)$statement->errorCode() > 0){
			//prepareが失敗
			throw new Exception(join(":",$statement->errorInfo()));
		}

		$result = $statement->execute();
		if((int)$statement->errorCode()> 0){
			throw new Exception(join(":",$statement->errorInfo()));
		}
		
		if($query->getMethod() == 0x01){
			//select

			$schema = ORM_Manager_Schema::get($query->getFrom()->getName());
			$record_base = ORM_Manager_Record::get($query->getFrom()->getName());

			$statement->setFetchMode(PDO::FETCH_NUM);
			$number = $statement->columnCount();

			$meta = array();
			for($i=0;$i<$number;$i++){
				$meta[] = $statement->getColumnMeta($i);
			}

			$collection = clone ORM_Manager_Collection::get($query->getFrom()->getName());
			$last = null;
			$relations = $schema->getRelations();
			
			while($row = $statement->fetch()){
				$record = clone $record_base;
				$this->hydration($schema,$meta,$row, $record,$last);

				$collection->append($record);
				$last = $record;
			}
			
			return $collection;

		}else if($query->getMethod() == 0x02){
			//insert
			if($result){
				$id = $this->getSession()->lastInsertId();
				return (!$id) ? $id : $result;

			}else{
				return false;
			}
			
		}
	}

	/**
	 *
	 * Schemaを元にデータを綺麗にまとめる
	 */
	public function hydration(ORM_Schema $schema,$meta, $row, ORM_Record &$record, ORM_Record &$last=null){
  	$table_name = $schema->getName();

  	//Todo: Relactionはクラスで返す。
		$relations = $schema->getRelations();
		$childs = array();

    foreach($meta as $offset=>$field):
      $name = $field['name'];
      if($table_name == $field['table'] || empty($field['table'])){
      	//メインテーブルはそのままRecordに突っ込む
        $record->$name = $row[$offset];
      }else{
      	//Relationは処理が異なるのでとりあえず配列にぶっこんでおく。
        $child = $field["table"];
        if(!isset($childs[$child])){
        	$child_record = clone ORM_Manager_Record::get($child);
        	$childs[$child] = $child_record;
        	$childs[$child]->$name = $row[$offset];
        }else{
        	$childs[$child]->$name = $row[$offset];
        }
      }
    endforeach;

    if($childs){
    	// key がorigin
			$is_same = false;
			if($last){
	    	foreach($schema->getPrimaryKeys() as $p_key => $schma){
	    		if($last->$p_key == $record->$p_key){
	    			$is_same = true;
	    		}else{
	    			$is_same = false;
	    		}
	    	}
	    }

	    foreach($childs as $key => $child){
	    	if($relations[$key]["type"] == "has_many"){
	    		if($is_same){
						if($record->$key instanceof ORM_Collection){
			    		$record->$key->append($child);
			    	}else{
			    		throw new Exception("something wrong");
			    	}
			    }else{
						$collection = clone ORM_Manager_Collection::get($key);
		    		$record->$key = $collection;
		    		$record->$key->append($child);
			    }
	    	}else{
	    		// has_one
	    		if(isset($record->$key)){
	    			throw new Exception("something wrong");
	    		}else{
		    		$record->$key = $child;
	    		}
	    	}
	    }

	  }
  }
	
	public function desc($name){
		// とりあえず用。
		$result = $this->getSession()->query("desc " . $name);
		if($result){
			$result->setFetchMode(PDO::FETCH_OBJ);

			//Todo: ここらへんはドライバ依存だと思うんで抽象化
			// したいと思うけどDriver別だからいらんかな？(´・ω・｀)
			$schema = new ORM_Schema($name);
			while($row = $result->fetch()){
				$schema->{$row->Field} = array(
					"type"=>$row->Type,
					"primary"=> ($row->Key == "PRI") ? true : false,
					"required" => ($row->Null != "YES") ? true : false,
					"autoincrement" => ($row->Extra == "auto_increment") ? true : false,
					"default" => $row->Default
				);
			}
			
			//Todo: Queryタイプにあったセッションを拾うように
			$schema->setWriteSession($this->getSession());
			ORM_Manager_Schema::attach($schema);

			return $schema;

		}else{
			throw new Exception("Schema Not Found");
		}
	}
	
	
	/**
	 *
	 * 接続したデータベースにあるテーブルをひろう。
	 * 
	 * Todo: DriverのInterfaceにする。
	 */
	public function getTables(){
		$sth = $this->getSession()->query("show tables");
		if($sth){
			$sth->setFetchMode(PDO::FETCH_COLUMN,0);
			return $sth ->fetchAll();
		}else{
			return false;
		}
	}
	

	private function process_limit(ORM_Query $query){
		//Todo: ドライバごとによしなにできるように
		$limit = $query->getLimit();
		return $limit->getOffset() . ","  . $limit->getLimit();
	}
	

	private function process_set(ORM_Query $query){
		return $query->getSet();
	}
	
	private function process_select(ORM_Query $query){
		return $query->getSelect();
	}

	private function process_from(ORM_Query $query){
		return $query->getFrom()->getStatement();
	}

	private function process_where(ORM_Query $query){
		$raw = "";
		$last = null;
		foreach($query->getWhere() as $where){
			if($last){
				$raw .= $last->getGlue();
			}
			// Todo: Driver依存に変更する
			$raw .= $where->getStatement();

			$last = $where;
		}
		return $raw;
	}

	private function process_join(ORM_Query $query){
		$raw = "";
		$last = null;
		foreach($query->getJoin() as $join){
			if($last){
				$raw .= " and ";
			}
			// Todo: Driver依存に変更する
			$raw .= $join->getStatement();

			$last = $join;
		}
		return $raw;
	}

	/**
	*
	* QueryクラスからRawQueryを作成する
	*
	* 要検討： ORM_QueryをTreeにして書いたほうが楽そう？
	**/
	public function FreezeQuery(ORM_Query $query){
		$this->getTables();
		if($query->getMethod() == 0x01){
			$raw = "select " . $this->process_select($query) . " from " . $this->process_from($query);

			if($query->getState() & ORM_Query::HAS_JOIN){
				$raw .= " left join " . $this->process_join($query);
			}

			if($query->getState() & ORM_Query::HAS_WHERE){
				$raw .= " where " . $this->process_where($query);
			}

			if($query->getState() & ORM_Query::HAS_LIMIT){
				$raw .= " limit " . $this->process_limit($query);
			}
			return $raw;
		}else if($query->getMethod() == 0x02){
			$raw = "insert " . $this->process_from($query) . " set " . $this->process_set($query);

			if($options = $query->getDriverOptions()){
				foreach($options as $option_id => $option){
					if($option_id == self::USE_DUPLICATE_KEY_UPDATE){
						$raw .= " on duplicate key update " . $option;
					}
				}
			}
			return $raw;

		}else if($query->getMethod() == 0x03){
			$raw = "update " . $this->process_from($query) . " set " . $this->process_set($query);
			if($query->getState() & ORM_Query::HAS_WHERE){
				$raw .= " where " . $this->process_where($query);
			}

			return $raw;
		}else if($query->getMethod() == 0x04){
			$raw = "delete from " . $this->process_from($query);
			if($query->getState() & ORM_Query::HAS_WHERE){
				$raw .= " where " . $this->process_where($query);
			}

			return $raw;
		}

	}
}
