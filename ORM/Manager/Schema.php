<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * Schemaを管理・生成します
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Manager_Schema{
	static $instance;

	protected $array;

	private function __construct(){
	}

	public static function getInstance(){
		if(!self::$instance){
			self::$instance = new ORM_Manager_Schema();
			return self::$instance;
		}

		return self::$instance;
	}

	public static function contains($name){
		$manager = self::getInstance();
		if(isset($manager->array[$name])){
			return true;
		}else{
			return false;
		}
	}

	public static function attach(ORM_Schema $schema){
		$manager = self::getInstance();
		$manager->array[$schema->getName()] = $schema;
		return $schema;
	}

	public static function get($name){
		$manager = self::getInstance();
		if($manager->contains($name)){
			//ロード済み
			return $manager->array[$name];
		}else if(class_exists("Schema_" . ORM_Util::Cameraized($name))){
			//定義済みのSchemaClassが有る
			$classname = "Schema_" . ORM_Util::Cameraized($name);
			return self::attach(new $classname());
		}else{
			//DriverからSchemaを自動生成する
			$session = ORM_Manager_Session::getConnection();
			return $session->loadDriver()->desc($name);
		}
	}

}