<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * ユーザー定義のCollectionを呼ぶための代理クラス
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Manager_Collection{
	protected $array;
	static $instance;
	
	public static function getInstance(){
		if(isset(self::$instance)){
			return self::$instance;
		}else{
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 *
	 * ユーザー定義のCollectionがあればそのClass名を、なければDefaultのClass名を返す。
	 */
	public static function getCollectionClass($lower_tablename){
		$classname = "Collection_" .  ORM_Util::Cameraized($lower_tablename);
		return (class_exists($classname)) ? $classname : "ORM_Collection";
	}

	/**
	 *
	 * Clone用のCollectionクラスを渡す。
	 */
	public static function get($lower_tablename){
		$instance = self::getInstance();
		if(isset($instance->array[$lower_tablename])){
			return $instance->array[$lower_tablename];
		}else{
			$classname = self::getCollectionClass($lower_tablename);
			
			$instance->array[$lower_tablename] = new $classname($lower_tablename);

			return $instance->array[$lower_tablename];
		}
	}
}
