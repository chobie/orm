<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * 複数Schemaを管理します
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Manager_Session{
	static $instance;
	protected $sessions;
	protected $names;

	private function __construct(){
		$this->sessions = array();
		$this->names = array();
	}
	
	public static function getConnection($dsn = null){
		$manager = self::getInstance();
		foreach($manager->sessions as $session){
			return $session;
		}
	}

	public static function getInstance(){
		if(isset(self::$instance)){
			return self::$instance;
		}else{
			$sm = new self();
			self::$instance = $sm;
			return $sm;
		}
	}
	
	public function attach($dsn,$name){
		if(isset($this->sessions[$name])){
			return $this->names[$name];
		}else if(isset($this->sessions[$dsn])){
			return $this->sessions[$dsn];
		}else{
			$session = new ORM_Session($dsn,$name);
			$this->sessions[$dsn] = $session;
			$this->names[$name] = $session;

			return $session;
		}
	}
}
