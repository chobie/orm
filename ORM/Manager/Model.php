<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * ユーザー定義のModelを呼ぶための代理クラス
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Manager_Model{

	public static function getModelClass($lower_tablename){
		$classname = "Model_" .  ORM_Util::Cameraized($lower_tablename);
		return (class_exists($classname)) ? $classname : "ORM_Model";
	}
}
