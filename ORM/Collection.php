<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * Collection
 *
 * ORM_Recordの集合です
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Collection implements Iterator, Countable{
  protected $position = 0;
  protected $array;
	protected $count = 0;

	public function save(){}
	public function delete(){}

  public function current(){
    return $this->array[$this->position];
  }

	// Recordを追加する
  public function append(ORM_Record $row){
    if((bool)$row){
      $this->array[] = $row;
      $this->count++;
      return true;
    }else{
      return false;
    }
  }

  public function key(){
    return $this->position;
  }

  public function next(){
    ++$this->position;
  }

  public function rewind(){
    $this->position = 0;
  }

  public function valid(){
    return isset($this->array[$this->position]);
  }
  
  public function count(){
    return $this->count;
  }
}