<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * Recordは単一の結果業セットをオブジェクト化したものです。
 *
 * ほぼPOPOで更新などの処理はSchemaを媒介して行ないます。
 *
 * Todo: Serializableの実装。
 *  ⇒UserRecordをSerializeしてSessionに入れて戻して$user->hoge()とかできると楽
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Record{
	protected $__schema__;
	
	public function __construct($schema_name = null){
		$classname = get_class($this);

		if($classname == get_class()){
			if(empty($schema_name)){
				throw new Exception("{$classname}::_construct required \$schema_name parameter");
			}
			$this->__schema__ = ORM_Util::UnCameraized($schema_name);
		}else{
			list($dummy,$name) = explode("_",$classname,2);
			$this->__schema__ = ORM_Util::UnCameraized($name);
			if(method_exists($this,"__setup__")){
				$this->__setup__();
			}
		}
	}
	
	//ユーザー定義Schema用
	protected function __setup__(){
		$schema = $this->getSchema();
		foreach($schema->getColumn(ORM_Schema::FILTER_THRU) as $key=>$column){
			if(!isset($this->$key)){
				$this->$key = null;
			}
		}
	}
	
	protected function getSchema(){
		return ORM_Manager_Schema::get($this->__schema__);
		
	}
	
	public function save(){
		$schema = $this->getSchema();
		$session = $schema->getWriteSession();

    $where = array();
    foreach($schema->getPrimaryKeys() as $key=>$type){
    	$where[] = $key . " = " . $this->$key;
    }

		//Todo: Queryは直接呼びたくないなー
		$query = new ORM_Query($session);
		$result = $query->select("1")->from($schema->getName())->where(join(" and ",$where))->execute();

		if($result){
			$query = new ORM_Query($session);

	    foreach($schema->getColumn() as $column=>$attributes){
	      if(isset($this->$column)){
	        $fields[] = $column. " = " . $session->quote($this->$column);
	      }
	    }

	    $query->set($fields)->update($schema->getName());
	    $query->where(join(" and ",$where));
			$query->execute();
		}
	}

	public function insert(){
		$schema = $this->getSchema();
		$session = $schema->getWriteSession();
		
		//Todo: Queryは直接呼びたくないなー
		$query = new ORM_Query($session);

		$sets = array();
		foreach($schema->getColumn(ORM_Schema::FILTER_NO_AUTOINCREMENT) as $column=>$type){
			if(isset($this->$column)){
				$sets[] = $column . " = " . $session->quote($this->$column);
			}
		}

		$result = $query->set(join(" , ",$sets))->insert($schema->getName())->execute();
		
	}

	public function delete(){
		$schema = $this->getSchema();
		$session = $schema->getWriteSession();

    $where = array();
    foreach($schema->getPrimaryKeys() as $key=>$type){
    	$where[] = $key . " = " . $this->$key;
    }

		//Todo: Queryは直接呼びたくないなー
		$query = new ORM_Query($session);
    $query->delete($schema->getName());
    $query->where(join(" and ",$where));

		return $query->execute();
	}
}
