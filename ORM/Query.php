<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * 抽象的なObjectベースのQuery
 *
 * 何を対象にどうするか？ということのみをメタデータとして持ち
 * 実際の作業に関しては全てDriverにゆだねます（今後）。
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Query{
	const CONCAT_AND = 0x01;
	const CONCAT_OR = 0x02;
	/*
	* 0x01 => select
	* 0x02 => insert
	* 0x03 => update
	* 0x04 => delete
	*/
	protected $method;

	protected $driver_options = array();

	protected $select;
	protected $from;
	protected $where;
	protected $join;

	protected $state = 0x00;
	/*
		selectの場合
		0x00 => なにもない
		0x01 => select 完了
		0x02 => from 完了
			以降オプション
		0x04 => where 完了
		0x08 => order 完了
		0x10 => join 完了
		0x20 => limit 完了
		
	*/
	
	const HAS_WHERE = 0x04;
	const HAS_JOIN = 0x10;
	const HAS_LIMIT = 0x20;
	

	protected $session_ref;
	
	
	/**
	*
	* queryの種別を取る
	**/
	public function getMethod(){
		return $this->method;
	}
	
	public function getDriverOptions(){
		return $this->driver_options;
	}
	
	public function getJoin(){
		return $this->join;
	}
	public function getLimit(){
		return $this->limit;
	}

	public function getSelect(){
		return $this->select;
	}
	public function getFrom(){
		return $this->from;
	}
	public function getSet(){
		return $this->set;
	}

	/**
	* Queryは直接つくるのはやめるべきだわな。
	* 	⇒Fatな感じになりやすいんで
	**/
	public function __construct(ORM_Session $session){
		$this->session_ref = $session;
	}

	public function getState(){
		return $this->state;
	}
	
	protected function clearState(){
		$this->state = 0;
	}

	protected function setState($int){
		$this->state = $int;
	}
	
	protected function addState($int){
		$this->setState( $this->getState() | $int );
	}
	
	public function getWhere(){
		return $this->where;
	}
	
	public function group(){
	}


	public function limit($limit,$offset=0){
		$this->addState(self::HAS_LIMIT);
		$this->limit = new ORM_Query_Limit($limit,$offset);
		return $this;
	}


	public function from($table){
		$this->from = new ORM_Query_From($table);
		return $this;
	}

	public function where($statement, $params = array(), $concat_type = self::CONCAT_AND){
		$this->addState(self::HAS_WHERE);
		$this->where[] = new ORM_Query_Where($statement,$params,$concat_type);

		return $this;
	}
	
	public function join($schema_name,$relations = array()){
		if(!(bool)$relations){
		}
		$this->join[] = new ORM_Query_Join($schema_name,$relations);
		$this->addState(self::HAS_JOIN);

		return $this;
	}
	
	public function set($statement, $params = array()){
		if(is_array($statement)){
			$this->set = join(" , ",$statement);
		}else{
			$this->set= $statement;
		}

		return $this;
	}
	
	public function order($statement){

		return $this;
	}
	
	public function select($statement,$driver_options = array()){
		$this->method = 0x01;
		$this->select = $statement;

		$this->driver_options = $driver_options;
		return $this;
	}
	
	public function insert($table,$driver_options = array()){
		$this->method = 0x02;
		$this->from = new ORM_Query_From($table);
		$this->driver_options = $driver_options;

		return $this;
	}
	
	public function update($table,$driver_options = array()){
		$this->method = 0x03;
		$this->from = new ORM_Query_From($table);
		$this->driver_options = $driver_options;
		return $this;
	}
	
	public function delete($table){
		$this->method = 0x04;
		$this->from = $table;
		return $this;
	}


	/**
	*
	* Driverに対してクエリを実行するように指示して
	* もろもろ行って結果を返す
	**/
	public function execute($hydrate_mode = ORM_Driver::HYDRATE_CLASS){
		return $this->getSession()
							->loadDriver()
							->execute($this,$hydrate_mode);
	}
	

	public function getSession(){
		return $this->session_ref;
	}


	/**
	*
	* DriverにQueryを作れ！という
	* どんなクエリなのか知りたい時につかう
	**/
	public function freeze(){
		return $this->getSession()->loadDriver()->FreezeQuery($this);
	}
}
