<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * Just Autoloader
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Autoloader{
  protected $prefixes = array();

	public function __construct(array $dirs = array()){
		$this->dirs = $dirs;
	}
  public function registerPrefixes(array $classes)
  {
    $this->prefixes = array_merge($this->prefixes, $classes);
    return $this;
  }

  public function registerPrefix($prefix, $path)
  {
    $this->prefixes[$prefix] = $path;
    return $this;
  }

  public function register()
  {
    spl_autoload_register(array($this, 'loadClass'));
  }

  public function loadClass($class)
  {
    $class_name = str_replace('_', DIRECTORY_SEPARATOR, $class);
    foreach ($this->prefixes as $prefix => $dir)
    {
      if (0 === strpos($class, $prefix))
      {
        $file = $dir.DIRECTORY_SEPARATOR. $class_name .'.php';
        if (is_file($file))
        {
          require $file;
        }

        return;
      }
    }
    return false;
  }
}
