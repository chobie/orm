<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * Schema間のRelationを定義したもの。
 *
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM_Relation{
	
	public function getRelationType(){
		/**
		 * has one or has many
		 */
	}
	
	public function getLocalKey(){
	}

	public function getForeignKey(){
	}
}
