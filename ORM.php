<?php
/*
 *
 * This file is part of the chobie's ORM Class Library
 *
 * (c) chobie <http://twitter.com/chobi_e>
 * 
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */


/**
 * ORM_Modelを呼ぶためのFactory
 *
 * @author chobie <http://twitter.com/chobi_e>
 *
 */
class ORM{
	protected $session;

	public function __construct(ORM_Session $session){
		$this->session = $session;
	}

	public function getModel($tablename){
		$class = ORM_Manager_Model::getModelClass($tablename);
		return new $class(ORM_Util::UnCameraized($tablename),$this->session);
	}
}
