<?php
require "ORM/Autoloader.php";
echo "<pre>";

$al = new ORM_Autoloader();
$al->registerPrefix("ORM",__DIR__);
$al->registerPrefix("Schema",__DIR__ . "/lib");
$al->registerPrefix("Collection",__DIR__ . "/lib");
$al->registerPrefix("Record",__DIR__ . "/lib");
$al->registerPrefix("Model",__DIR__ . "/lib");
$al->register();
$start = microtime(true);
try{
	$sm = ORM_Manager_Session::getInstance();

	//dsn , aliasとりあえず
	$session = $sm->attach("mysql://root@localhost/chobie","uhi");
	//とりあえず。そのうちカッコイイ呼び方にする。
	$orm = new ORM($session);
	$model = $orm->getModel("Test");
	
	$record = $model->getRecord();
	$record->name = "unu";
	//$record->insert();

	//Model::findは単純なandのクエリしか行いません。難しいのはModelに実装してね(´・ω・｀)
	$list = $model->findOne();
	//$list = $model->find();
	var_dump($list);
	
	$i = 0;
	//foreach($list as $r){
		//$r->name = "monu" . $i;
		//$r->delete();
		$i++;
	//}

	
}catch(Exception $e){
	var_dump($e->getMessage());
}
$end = microtime(true);

echo "</pre>";
var_dump($end-$start);